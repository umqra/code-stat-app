import * as brace from "brace";
import "brace/mode/java";

export class CustomHighlightRules extends brace.acequire("ace/mode/text_highlight_rules").TextHighlightRules {
    constructor() {
        super();
        var keywords = (
            "and|as|assert|break|class|continue|def|del|elif|else|except|exec|" +
            "finally|for|from|global|if|import|in|is|lambda|not|or|pass|print|" +
            "raise|return|try|while|with|yield|async|await|nonlocal|function|class|where"
        );

        var builtinConstants = (
            "True|False|None|NotImplemented|Ellipsis|__debug__"
        );

        var builtinFunctions = (
            "abs|divmod|input|open|staticmethod|all|enumerate|int|ord|str|any|" +
            "eval|isinstance|pow|sum|basestring|execfile|issubclass|print|super|" +
            "binfile|bin|iter|property|tuple|bool|filter|len|range|type|bytearray|" +
            "float|list|raw_input|unichr|callable|format|locals|reduce|unicode|" +
            "chr|frozenset|long|reload|vars|classmethod|getattr|map|repr|xrange|" +
            "cmp|globals|max|reversed|zip|compile|hasattr|memoryview|round|" +
            "__import__|complex|hash|min|apply|delattr|help|next|setattr|set|" +
            "buffer|dict|hex|object|slice|coerce|dir|id|oct|sorted|intern|" +
            "ascii|breakpoint|bytes|string|boolean"
        );

        const decimalInteger = "(?:(?:[1-9]\\d*)|(?:0))";
        var keywordMapper = this.createKeywordMapper({
            "support.function": builtinFunctions,
            "constant.language": builtinConstants,
            "keyword": keywords
        }, "identifier");
        this.$rules = {
            start: [
                {
                    token: "comment",
                    regex: "#.*$"
                },
                {
                    token: "string",
                    regex: '".*?"'
                },
                {
                    token: "constant.numeric", // imaginary
                    regex: decimalInteger,
                },
                {
                    token: keywordMapper,
                    regex: "[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
                }
            ]
        };
    }
}

export default class CustomCodeStyleMode extends brace.acequire("ace/mode/java").Mode {
    constructor() {
        super();
        this.HighlightRules = CustomHighlightRules;
    }
}