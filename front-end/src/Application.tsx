import * as React from "react";
import {Route, Switch, withRouter} from "react-router";
import CodeTypingStage, {ICodeTypingStageProps, IStageProps} from "./code-typing-stage/CodeTypingStage";
import {createGlobalStyle} from 'styled-components';
import Stats from "./stats/Stats";
import styled from "styled-components";
import {IBackendApi} from "./BackendApi";
import {IStatementProps} from "./code-typing-stage/Statement";

const HorizontalDiv = styled.div`
  display: flex;
  flex-direction: row;
`;

const PadDiv = styled.div`
  padding: ${(props: { pad: number }) => props.pad}px;
`;

interface IApplicationProps {
    backendApi: IBackendApi;
    sessionId: string;
}

interface IApplicationState {
    stages: IStageProps[];
}

class Application extends React.Component<IApplicationProps, IApplicationState> {
    public state = {
        stages: [
            {
                stageCode: 'loading',
                statement: {
                    title: "Content is loading",
                    content: ""
                },
                nextStageButton: "Wait",
                noCode: true
            }]
    };

    async componentDidMount() {
        const {backendApi} = this.props;
        const stageCodes = await backendApi.getStages();
        const stages = stageCodes.map(stage => backendApi.getStage(stage));
        Promise.all(stages).then(result => this.setState({stages: result}));
    }

    public render(): React.ReactNode {
        const {backendApi, sessionId} = this.props;
        const {stages} = this.state;
        return (
            <>
                <PadDiv pad={100}>
                    <HorizontalDiv>
                        <Switch>
                            <Route exact path="/" render={() =>
                                <CodeTypingStage stages={stages}
                                                 onCodeSave={(stageCode, code) => backendApi.saveCode(stageCode, sessionId, code)}
                                                 onCodeSubmit={(stageCode, code) => backendApi.sendCode(stageCode, sessionId, code)}/>}/>
                            <Route exact path="/stats" render={() =>
                                <Stats stages={stages}
                                       getStageStats={(stageCode) => backendApi.getStats(stageCode, 5)}/>}/>
                        </Switch>
                    </HorizontalDiv>
                </PadDiv>
            </>
        );
    }
}

// @ts-ignore
export default withRouter(Application);
