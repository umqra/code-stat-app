import * as React from "react";
import styled from "styled-components";
import './Statement.css';

export interface IStatementProps {
    title: string;
    content: React.ReactNode;
}

const StatementTitle = styled.h1`
  font-size: 2em;
`;

const StatementContent = styled.div`
  font-size: 1em;
  & p {
    text-indent: 1em;
  }
  max-width: 700px;
`;

export default class Statement extends React.PureComponent<IStatementProps> {
    public render(): React.ReactNode {
        const { title, content } = this.props;
        return (
            <>
                <StatementTitle>{title}</StatementTitle>
                <StatementContent>{content}</StatementContent>
            </>);
    }
}
