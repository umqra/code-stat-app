import * as React from "react";
import {withRouter} from "react-router";
import {repeat} from "../utils";
import Statement, {IStatementProps} from "./Statement";
import Navigation from "./Navigation";
import StatementWithCode from "./StatementWithCode";
import styled from "styled-components";
import SubmitButton from "../SubmitButton";

export interface IStageProps {
    stageCode: string;
    statement: IStatementProps;
    nextStageButton: string;
    noCode: boolean | null;
}

export interface ICodeTypingStageProps {
    stages: IStageProps[];
    onCodeSubmit: (stageCode: string, code: string) => void;
}

interface ICodeTypingStageState {
    codes: string[];
    currentStatementIndex: number;
}

const FlexDiv = styled.div`
  flex-grow: ${(props: {grow: number}) => props.grow};
`;

const MarginLeftDiv = styled.div`
  margin-left: ${(props: {pad: number}) => props.pad}em;
`;

class CodeTypingStage extends React.Component<ICodeTypingStageProps, ICodeTypingStageState> {
    constructor(props) {
        super(props);
        this.handleStatementChange = this.handleStatementChange.bind(this);
        this.renderStage = this.renderStage.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            codes: repeat("", this.props.stages.length),
            currentStatementIndex: 0,
        };
    }
    static getDerivedStateFromProps(props: ICodeTypingStageProps, state: ICodeTypingStageState) {
        if (props.stages.length != state.codes.length) {
            return {
                codes: repeat("", props.stages.length),
                currentStatementIndex: 0,
            };
        }
        return null;
    }
    public render(): React.ReactNode {
        const { stages } = this.props;
        const { currentStatementIndex, codes } = this.state;
        const filledStages = stages
            .map((stage, index) => {
                if (codes[index] !== "" || stage.noCode)
                    return index;
                return -1;
            })
            .filter(index => index != -1);
        return (<>
                <FlexDiv grow={0}>
                    <Navigation titles={stages.map((x) => x.statement.title)}
                                currentStatementIndex={currentStatementIndex}
                                filledStages={filledStages}
                                onStatementChange={this.handleStatementChange}/>
                </FlexDiv>
                <FlexDiv grow={0}>
                    <MarginLeftDiv pad={5}>
                        {this.renderStage(this.currentStatement)}
                    </MarginLeftDiv>
                </FlexDiv>
            </>);
    }
    get currentStatement(): IStatementProps | null {
        const { currentStatementIndex } = this.state;
        const { stages } = this.props;
        return currentStatementIndex == null ? null : stages[currentStatementIndex].statement;
    }

    private handleStatementChange(newStatement) {
        const { stages } = this.props;
        const targetStatementIndex = stages.findIndex((value) => value.statement.title === newStatement);
        this.setState({currentStatementIndex: targetStatementIndex});
    }

    private handleSubmit() {
        const { stages, onCodeSubmit } = this.props;
        const { currentStatementIndex, codes } = this.state;
        if (currentStatementIndex == null) {
            return;
        }
        onCodeSubmit(stages[currentStatementIndex].stageCode, codes[currentStatementIndex]);
        if (currentStatementIndex + 1 == stages.length) {
            // @ts-ignore
            this.props.history.push('/stats');
            return;
        }
        this.setState((prevState) => ({
            currentStatementIndex: prevState.currentStatementIndex + 1,
        }));
    }

    private renderStage(statement: IStatementProps): React.ReactNode {
        const { codes } = this.state;
        const { stages } = this.props;
        const statementIndex = stages.map((x) => x.statement).indexOf(statement);
        const stage = stages[statementIndex];
        let components = [];
        if (stage.noCode) {
            components = [(<Statement {...stage.statement}/>)];
        }
        else {
            components = [
                (<StatementWithCode {...statement} code={codes[statementIndex]} onCodeChange={(newCode) => {
                    this.setState((prevState) => {
                        const newCodes = prevState.codes.slice();
                        newCodes[prevState.currentStatementIndex] = newCode;
                        return {
                            codes: newCodes
                        };
                    });
                }}/>)
            ];
        }
        return (
            <div>
                {components}
                <div style={{textAlign: "center"}}>
                    {(<SubmitButton onClick={this.handleSubmit} text={stage.nextStageButton}/>)}
                </div>
            </div>
        );
    }
}

// @ts-ignore
export default withRouter(CodeTypingStage);
