import * as React from "react";
import styled from "styled-components";

interface INavigationTabProps {
    isAlreadyUsed: boolean;
    isSelected: boolean;
    title: string;
    onClick: () => void;
}

const NavigationTabButton = styled.button`
  background-color: white;
  color: ${(props: { isAlreadyUsed: boolean, isSelected: boolean }) => {
    if (props.isSelected)
        return "darkorange";
    if (props.isAlreadyUsed)
        return "green";
    return "lightgray";
}};
  margin: 1em;
  padding: 0.25em 1em;
  border: none;
  border-radius: 3px;
  text-align: left;
  &:before {
    content: "\\2022";
    width: 1.5em;
    display: inline-block;
  }
  &:hover {
    cursor: pointer;
    text-decoration: underline;
  }
  &:focus {
    outline: none;
  }
  &::-moz-focus-inner {
    border:0;
  }
`;

class NavigationTab extends React.Component<INavigationTabProps> {
    public render(): React.ReactNode {
        const { isSelected, isAlreadyUsed, title, onClick } = this.props;
        return (<NavigationTabButton isAlreadyUsed={isAlreadyUsed} isSelected={isSelected} onClick={onClick}>{title}</NavigationTabButton>);
    }
}

export default NavigationTab;
