import * as React from "react";
import CodeArea from "../CodeArea";
import Statement, { IStatementProps } from "./Statement";
import styled from "styled-components";

interface IStatementWithCodeProps extends IStatementProps {
    code: string;
    onCodeChange: (newCode: string) => void;
}

const ExpandDiv = styled.div`
  display: block;
`;

class StatementWithCode extends React.Component<IStatementWithCodeProps> {
    public render(): React.ReactNode {
        const { title, content, code, onCodeChange } = this.props;
        return (
            <ExpandDiv>
                <Statement title={title} content={content}/>
                <CodeArea disabled={false} text={code} onChange={onCodeChange}/>
            </ExpandDiv>
        );
    }
}

export default StatementWithCode;
