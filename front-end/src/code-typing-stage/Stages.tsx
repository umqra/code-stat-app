import * as React from "react";
import styled from "styled-components";

export const GettingStartedStage = "getting-started";
export const EmptyStringCodeStage = "empty-string";
export const LeftPageStage = "left-pad";
export const GcdStage = "gcd";
export const FinalStatStage = "final-stat";

const Exercise = styled.div`
  padding: 1em;
  margin: 1em;
  border-left: 1px solid gray;
  color: gray;
`;

export default [
    {
        statement: {
            content: (
            <div>
                <p>This is a small research about how people writing the code</p>
                <p>One day I sad that if you ask 15 programmers to write a GCD function, than you probably get only 3 or 5 different versions of code with naming and spaces considering</p>
                <p>This may be true statement in my surroundings because I mostly connected with programmers that have some similarties in code style with me</p>
                <p>Anyway, I decided to test this hypothesis worldwide</p>
            </div>),
            title: "Getting started",
        },
        stageCode: GettingStartedStage,
        noCode: true,
        nextStageButton: "Go",
    },
    {
        statement: {
            content: (
            <div>
                <p>First exercise is pretty simple:</p>
                <Exercise>
                    <p>Imagine you have a string <code>S</code> and you want to check is this string contains only whitespace characeters or not.
                    Write a function that implements this logic.</p>
                    <p><b>You are free to write function in any language you like with any variables naming and styling</b></p>
                </Exercise>
                <p>Write this expression in the area below</p>
            </div>
            ),
            title: "Empty string",
        },
        stageCode: EmptyStringCodeStage,
        nextStageButton: "Next"
    },
    {
        statement: {
            content: (<p>Second task is more complicated: write a function that pads input string with zeros to the specified length</p>),
            title: "Left pad",
        },
        stageCode: LeftPageStage,
        nextStageButton: "Next"
    },
    {
        statement: {
            content: (<p>If you don't like this task you can just skip it. In other case - implement simple function that calculate GCD of two numbers</p>),
            title: "A little bit of math",
        },
        stageCode: GcdStage,
        nextStageButton: "Finish"
    }
];
