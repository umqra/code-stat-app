import * as React from "react";
import NavigationTab from "./NavigationTab";
import styled from "styled-components";

interface IStatementsNavigationProps {
    titles: string[];
    currentStatementIndex: number;
    filledStages: number[];
    onStatementChange: (title: string) => void;
}

const VerticalDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

class Navigation extends React.Component<IStatementsNavigationProps> {
    public handleClick(newTitle) {
        this.props.onStatementChange(newTitle);
    }
    public render(): React.ReactNode {
        const { titles, currentStatementIndex, filledStages } = this.props;
        return (
            <VerticalDiv>
                {titles.map((t, index) =>
                    <NavigationTab
                        key={t}
                        title={t}
                        isSelected={index == currentStatementIndex}
                        isAlreadyUsed={filledStages.indexOf(index) !== -1}
                        onClick={() => this.handleClick(t)}
                    />)
                }
            </VerticalDiv>
        );
    }
}

export default Navigation;
