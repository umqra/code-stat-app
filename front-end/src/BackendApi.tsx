import axios from "axios";
import * as React from "react";
import { IStageProps } from "./code-typing-stage/CodeTypingStage";

export interface ICodeStatistics {
    code: string;
    count: number;
    similarCodes: string[];
    comment: string;
}

interface IRawStatement {
    title: string;
    content: string;
}

interface IStageDescription {
    stageCode: string;
    statement: IRawStatement;
    nextStageButton: string;
    noCode: boolean | null;
}

export interface IBackendApi {
    getStages(): Promise<string[]>;
    getStage(stageCode: string): Promise<IStageProps>;
    sendCode(stageCode: string, sessionId: string, code: string): Promise<void>;
    saveCode(stageCode: string, sessionId: string, code: string): Promise<void>;
    getCode(stageCode: string, sessionId: string): Promise<string>;
    getStats(stageCode: string, limit: number): Promise<ICodeStatistics[]>;
}

export default class BackendApi implements IBackendApi {
    private serverAddress: string;
    constructor(serverAddress) {
        this.serverAddress = serverAddress;
    }
    public async getStages(): Promise<string[]> {
        const response = await this.makeGetRequest('stages');
        return response.data;
    }
    public async getStage(stageCode: string): Promise<IStageProps> {
        const rawStage = await this.getRawStage(stageCode);
        return {
            noCode: rawStage.noCode,
            nextStageButton: rawStage.nextStageButton,
            stageCode: rawStage.stageCode,
            statement: {
                title: rawStage.statement.title,
                content: (<div dangerouslySetInnerHTML={{__html: rawStage.statement.content}}/>),
            }
        };
    }
    private async getRawStage(stageCode: string): Promise<IStageDescription> {
        const response = await this.makeGetRequest(`stage/${stageCode}`);
        return response.data
    }
    public async sendCode(stageCode: string, sessionId: string, code: string): Promise<void> {
        const response = await this.makePostRequest('sendCode', {
            code: code,
            codeIdentifier: {
                stageCode: stageCode,
                sessionId: sessionId,
            }
        });
    }
    public async saveCode(stageCode: string, sessionId: string, code: string): Promise<void> {
        await this.makePostRequest('saveCode', {
            code: code,
            codeIdentifier: {
                stageCode: stageCode,
                sessionId: sessionId,
            }
        });
    }
    public async getCode(stageCode: string, sessionId: string): Promise<string> {
        const response = await this.makeGetRequest(`getCode/${stageCode}/${sessionId}`);
        return response.data;
    }

    public async getStats(stageCode: string, limit: number): Promise<ICodeStatistics[]> {
        const response = await this.makeGetRequest(`getStats/${stageCode}/${limit}`);
        return response.data;
    }

    public async makeGetRequest(path: string): Promise<any> {
        return await axios.get(`http://${this.serverAddress}/api/${path}`);
    }
    public async makePostRequest(path: string, data: any): Promise<any> {
        return await axios.post(`http://${this.serverAddress}/api/${path}`, data);
    }
}
