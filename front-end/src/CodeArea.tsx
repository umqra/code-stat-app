import * as React from "react";

import AceEditor from 'react-ace';

import 'brace/theme/github';
import CustomCodeStyleMode from "./CustomCodeStyleMode";

interface ICodeAreaProps {
    text: string;
    onChange: (text: string) => void;
    disabled: boolean;
}

class CodeArea extends React.Component<ICodeAreaProps> {
    componentDidMount() {
        const customMode = new CustomCodeStyleMode();
        // @ts-ignore
        this.refs.aceEditor.editor.getSession().setMode(customMode);
    }
    private getCustomProperties(): any {
        const { onChange, disabled, text } = this.props;
        if (disabled) {
            const textHeight = (text.trim().match(/\n/g) || []).length + 2;
            return {
                onChange: (newCode: string) => {},
                height: `${textHeight}em`,
                readOnly: true,
                fontSize: 14,
                highlightActiveLine: false,
                showPrintMargin: false,
                showGutter: false,
                setOptions: {
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: false,
                    showLineNumbers: false,
                }
            };
        }
        return {
            onChange: onChange,
            height: "7em",
            readOnly: false,
            fontSize: 18,
            highlightActiveLine: true,
            showPrintMargin: true,
            showGutter: true,
        }
    }
    public render(): React.ReactNode {
        const { text } = this.props;

        return (
            <AceEditor
                ref={"aceEditor"}
                mode="text"
                theme="github"
                name="CodeArea"
                value={text}
                {...this.getCustomProperties()}
            />
        );
    }
}

export default CodeArea;
