export function repeat(value: string, count: number): string[] {
    return Array(count).fill(value);
}
