import * as React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  background-color: white;
  padding: 0.5em 1em;
  border: 1px solid lightgray;
  border-radius: 2px;
  margin: auto 0;
  font-size: 1.2em;
  display: inline-block;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
  &:focus {
    outline: none;
  }
  &::-moz-focus-inner {
    border:0;
  }
`;

interface ISubmitButtonProps {
    text: string;
    onClick: () => void;
}

class SubmitButton extends React.Component<ISubmitButtonProps> {
    public render(): React.ReactNode {
        const { text, onClick } = this.props;
        return (
            <StyledButton onClick={onClick}>
                {text}
            </StyledButton>
        );
    }
}

export default SubmitButton;
