import React = require("react");
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
  &:focus, &:hover, &:visited, &:link, &:active {
    text-decoration: none;
    color: black;
  }
`;

export default (props) => <StyledLink {...props}/>;
