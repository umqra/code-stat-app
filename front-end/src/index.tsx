import * as React from "react";
import * as ReactDOM from "react-dom";
import { HashRouter } from "react-router-dom";
import Application from "./Application";
import BackendApi from "./BackendApi";
import GetSessionId from "./SessionManager";

const backendApi = new BackendApi("127.0.0.1:5000");
const sessionId = GetSessionId();

ReactDOM.render(
    <HashRouter>
        <Application backendApi={backendApi} sessionId={sessionId}/>
    </HashRouter>,
    document.getElementById("app"),
);
