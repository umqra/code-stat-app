import * as uuid from "uuid";
import * as store from "store";

export default function GetSessionId(): string {
    const storedSession = store.get("sessionId");
    if (storedSession != undefined) {
        console.log(`Found session ${storedSession} in a local storage`);
        return storedSession;
    }
    const generatedSession = uuid();
    console.log(`Can't find session in a local storage, generate new: ${generatedSession}`);
    store.set("sessionId", generatedSession);
    return generatedSession;
}