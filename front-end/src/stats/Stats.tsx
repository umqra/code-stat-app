import * as React from "react";
import {withRouter} from "react-router";
import {IStatementProps} from "../code-typing-stage/Statement";
import styled from "styled-components";
import {ICodeStatistics} from "../BackendApi";
import CodeArea from "../CodeArea";

interface IStageProps {
    stageCode: string;
    statement: IStatementProps;
    nextStageButton: string;
    noCode: boolean | null;
}

export interface IStatsProps {
    stages: IStageProps[];
    getStageStats: (stageCode: string) => ICodeStatistics[];
}

const StatementTitle = styled.h1`
  font-size: 2em;
`;

const FloatingDiv = styled.div`
  float: left;
  width: 25%;
  padding: 10px;
  box-sizing: border-box;
  overflow: hidden;
`;


class StageCodeStat extends React.Component<ICodeStatistics> {
    public render(): React.ReactNode {
        const { code, count } = this.props;
        return (
            <div>
                <CodeArea text={code} onChange={x => {}} disabled={true}/>
                <div>Count: {count}</div>
            </div>);
    }
}

interface IStageCodesStatProps {
    codeStats: ICodeStatistics[];
    stage: IStageProps;
}

class StageCodesStat extends React.Component<IStageCodesStatProps> {
    public render(): React.ReactNode {
        const { stage, codeStats } = this.props;
        return (
            <FloatingDiv>
                <StatementTitle>{stage.statement.title}</StatementTitle>
                {codeStats.map((p, index) => <StageCodeStat key={index} {...p}/>)}
            </FloatingDiv>
        );
    }

}

class Stats extends React.Component<IStatsProps> {
    constructor(props) {
        super(props);
    }
    public render(): React.ReactNode {
        const { stages, getStageStats } = this.props;
        const stageCodes = stages.map((stage) => {
            const stageStats = getStageStats(stage.stageCode);
            if (stageStats.length == 0) {
                return null;
            }
            return (
                <StageCodesStat
                    key={stage.stageCode}
                    codeStats={stageStats}
                    stage={stage}
                />
            );
        });
        return (
        <div style={{width: "100%"}}>
            <StatementTitle>Final statistics</StatementTitle>
            <div>
                {stageCodes}
            </div>
        </div>);
    }
}

// @ts-ignore
export default withRouter(Stats);
