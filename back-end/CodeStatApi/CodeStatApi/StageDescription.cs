namespace CodeStatApi
{
    public class StageDescription
    {
        public StageStatement Statement { get; set; }
        public string StageCode { get; set; }
        public bool NoCode { get; set; }
        public string NextStageButton { get; set; }
    }
}