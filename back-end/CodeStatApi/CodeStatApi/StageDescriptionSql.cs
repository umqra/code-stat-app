using System.ComponentModel.DataAnnotations;

namespace CodeStatApi
{
    public class StageDescriptionSql
    {
        [Key]
        public string StageCode { get; set; }
        public int StageOrder { get; set; }
        public string StatementTitle { get; set; }
        public string StatementContent { get; set; }
        public bool NoCode { get; set; }
        public string NextStageButton { get; set; }
    }
}