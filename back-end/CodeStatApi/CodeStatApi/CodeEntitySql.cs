using System;

namespace CodeStatApi
{
    public class CodeEntitySql
    {
        public string StageCode { get; set; }
        public Guid SessionId { get; set; }
        public string Code { get; set; }
    }
}