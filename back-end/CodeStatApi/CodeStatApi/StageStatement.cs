namespace CodeStatApi
{
    public class StageStatement
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}