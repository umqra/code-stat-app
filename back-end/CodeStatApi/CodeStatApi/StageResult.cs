namespace CodeStatApi
{
    public class StageResult
    {
        public CodeIdentifier CodeIdentifier { get; set; }
        public string Code { get; set; }
    }
}