using Microsoft.EntityFrameworkCore;

namespace CodeStatApi
{
    public class CodeStatContext : DbContext
    {
        public DbSet<StageDescriptionSql> StageDescriptions { get; set; }
        public DbSet<CodeEntitySql> Codes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // todo(sivukhin, 16.02.2019): fix this connection string
            optionsBuilder.UseNpgsql("Host=db;Database=database;Username=admin;Password=password");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CodeEntitySql>().HasKey(e => new {e.SessionId, e.StageCode});
            modelBuilder.Entity<StageDescriptionSql>().HasData(new StageDescriptionSql
                {
                    StatementContent = @"<div>
<p>This is a small research about how people writing the code</p>
<p>One day I sad that if you ask 15 programmers to write a GCD function, than you probably get only 3 or 5 different versions of code with naming and spaces considering</p>
<p>This may be true statement in my surroundings because I mostly connected with programmers that have some similarties in code style with me</p>
<p>Anyway, I decided to test this hypothesis worldwide</p>
</div>",
                    StageOrder = 0,
                    StatementTitle = "Getting started",
                    StageCode = "getting-started",
                    NoCode = true,
                    NextStageButton = "Go",
                },
                new StageDescriptionSql
                {
                    StatementContent = @"<div>
<p>First exercise is pretty simple:</p>
<div class=""exercise"">
<p>Imagine you have a string <code>S</code> and you want to check is this string contains only whitespace characeters or not.
Write a function that implements this logic.</p>
<p><b>You are free to write function in any language you like with any variables naming and styling</b></p>
</div>
<p>Write this expression in the area below</p>
</div>",
                    StageOrder = 1,
                    StatementTitle = "Empty string",
                    StageCode = "empty-string",
                    NextStageButton = "Next",
                },
                new StageDescriptionSql
                {
                    StatementContent =
                        @"<p>Second task is more complicated: write a function that pads input string with zeros to the specified length</p>",
                    StageOrder = 2,
                    StatementTitle = "Left pad",
                    StageCode = "left-pad",
                    NextStageButton = "Next",
                },
                new StageDescriptionSql
                {
                    StatementContent =
                        @"<p>If you don't like this task you can just skip it. In other case - implement simple function that calculate GCD of two numbers</p>",
                    StageOrder = 3,
                    StatementTitle = "A little bit of math",
                    StageCode = "gcd",
                    NextStageButton = "Finish",
                });
        }
    }
}