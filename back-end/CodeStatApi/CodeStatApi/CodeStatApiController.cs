using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace CodeStatApi
{
    [Route("api")]
    [ApiController]
    public class CodeStatApiController : Controller
    {
        [HttpGet("stages")]
        public ActionResult GetStageList()
        {
            using (var context = new CodeStatContext())
            {
                return Json(context.StageDescriptions.OrderBy(stage => stage.StageOrder).Select(stage => stage.StageCode).ToArray());
            }
        }

        [HttpGet("stage/{stageCode}")]
        public ActionResult GetStage(string stageCode)
        {
            using (var context = new CodeStatContext())
            {
                var stageDescription = context.StageDescriptions.SingleOrDefault(stage => stage.StageCode == stageCode);
                if (stageDescription == null)
                    return NotFound();
                return Json(new StageDescription
                {
                    StageCode = stageDescription.StageCode,
                    Statement = new StageStatement
                    {
                        Title = stageDescription.StatementTitle,
                        Content = stageDescription.StatementContent
                    },
                    NoCode = stageDescription.NoCode,
                    NextStageButton = stageDescription.NextStageButton
                });
            }
        }

        [HttpPost("sendCode")]
        public ActionResult SendCode(StageResult result)
        {
            // todo(sivukhin, 16.02.2019): Send code to the processing stage
            using (var context = new CodeStatContext())
            {
                context.Codes.Add(new CodeEntitySql
                {
                    StageCode = result.CodeIdentifier.StageCode,
                    SessionId = result.CodeIdentifier.SessionId,
                    Code = result.Code,
                });
                context.SaveChanges();
            }

            return Ok();
        }

        [HttpPost("saveCode")]
        public ActionResult SaveCode(StageResult result)
        {
            using (var context = new CodeStatContext())
            {
                context.Codes.Add(new CodeEntitySql
                {
                    StageCode = result.CodeIdentifier.StageCode,
                    SessionId = result.CodeIdentifier.SessionId,
                    Code = result.Code,
                });
                context.SaveChanges();
            }

            return Ok();
        }

        [HttpGet("getCode/{StageCode}/{SessionId}")]
        public ActionResult GetCode([FromRoute] CodeIdentifier codeIdentifier)
        {
            using (var context = new CodeStatContext())
            {
                var code = context.Codes.SingleOrDefault(c => c.SessionId == codeIdentifier.SessionId && 
                                                              c.StageCode == codeIdentifier.StageCode);
                if (code == null)
                    return NotFound();
                return Json(code);
            }
        }

        [HttpGet("getStats/{stageCode}")]
        public ActionResult GetStats(string stageCode, int? limit)
        {
            return Json(new string[0]);
        }
    }
}