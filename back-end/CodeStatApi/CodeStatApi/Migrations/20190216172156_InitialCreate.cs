﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeStatApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes",
                columns: table => new
                {
                    StageCode = table.Column<string>(nullable: false),
                    SessionId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes", x => new { x.SessionId, x.StageCode });
                });

            migrationBuilder.CreateTable(
                name: "StageDescriptions",
                columns: table => new
                {
                    StageCode = table.Column<string>(nullable: false),
                    StatementTitle = table.Column<string>(nullable: true),
                    StatementContent = table.Column<string>(nullable: true),
                    NoCode = table.Column<bool>(nullable: false),
                    NextStageButton = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StageDescriptions", x => x.StageCode);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes");

            migrationBuilder.DropTable(
                name: "StageDescriptions");
        }
    }
}
