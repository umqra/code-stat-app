﻿// <auto-generated />
using System;
using CodeStatApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CodeStatApi.Migrations
{
    [DbContext(typeof(CodeStatContext))]
    [Migration("20190216172156_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.1-servicing-10028")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("CodeStatApi.CodeEntitySql", b =>
                {
                    b.Property<Guid>("SessionId");

                    b.Property<string>("StageCode");

                    b.Property<string>("Code");

                    b.HasKey("SessionId", "StageCode");

                    b.ToTable("Codes");
                });

            modelBuilder.Entity("CodeStatApi.StageDescriptionSql", b =>
                {
                    b.Property<string>("StageCode")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("NextStageButton");

                    b.Property<bool>("NoCode");

                    b.Property<string>("StatementContent");

                    b.Property<string>("StatementTitle");

                    b.HasKey("StageCode");

                    b.ToTable("StageDescriptions");
                });
#pragma warning restore 612, 618
        }
    }
}
