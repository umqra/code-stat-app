﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeStatApi.Migrations
{
    public partial class StageOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StageOrder",
                table: "StageDescriptions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "empty-string",
                column: "StageOrder",
                value: 1);

            migrationBuilder.UpdateData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "gcd",
                column: "StageOrder",
                value: 3);

            migrationBuilder.UpdateData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "left-pad",
                column: "StageOrder",
                value: 2);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StageOrder",
                table: "StageDescriptions");
        }
    }
}
