﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeStatApi.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "StageDescriptions",
                columns: new[] { "StageCode", "NextStageButton", "NoCode", "StatementContent", "StatementTitle" },
                values: new object[,]
                {
                    { "getting-started", "Go", true, @"<div>
                <p>This is a small research about how people writing the code</p>
                <p>One day I sad that if you ask 15 programmers to write a GCD function, than you probably get only 3 or 5 different versions of code with naming and spaces considering</p>
                <p>This may be true statement in my surroundings because I mostly connected with programmers that have some similarties in code style with me</p>
                <p>Anyway, I decided to test this hypothesis worldwide</p>
                </div>", "Getting started" },
                    { "empty-string", "Next", false, @"<div>
                <p>First exercise is pretty simple:</p>
                <div class=""exercise"">
                <p>Imagine you have a string <code>S</code> and you want to check is this string contains only whitespace characeters or not.
                Write a function that implements this logic.</p>
                <p><b>You are free to write function in any language you like with any variables naming and styling</b></p>
                </div>
                <p>Write this expression in the area below</p>
                </div>", "Empty string" },
                    { "left-pad", "Next", false, "<p>Second task is more complicated: write a function that pads input string with zeros to the specified length</p>", "Left pad" },
                    { "gcd", "Finish", false, "<p>If you don't like this task you can just skip it. In other case - implement simple function that calculate GCD of two numbers</p>", "A little bit of math" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "empty-string");

            migrationBuilder.DeleteData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "gcd");

            migrationBuilder.DeleteData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "getting-started");

            migrationBuilder.DeleteData(
                table: "StageDescriptions",
                keyColumn: "StageCode",
                keyValue: "left-pad");
        }
    }
}
