using System;

namespace CodeStatApi
{
    public class CodeIdentifier
    {
        public string StageCode { get; set; }
        public Guid SessionId { get; set; }
    }
}