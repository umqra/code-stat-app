#!/bin/bash

set -e
>&2 echo "PostgreSQL Server is up - starting the api"
dotnet CodeStatApi.dll
